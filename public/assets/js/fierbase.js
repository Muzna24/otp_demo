
        $(document).ready(function() {

            const firebaseConfig = {
                apiKey: "AIzaSyAV0YVZEZNuG9z2cssTq0Z3Z8DUa7z-Ksg",
                authDomain: "otpapp-8f84f.firebaseapp.com",
                projectId: "otpapp-8f84f",
                storageBucket: "otpapp-8f84f.appspot.com",
                messagingSenderId: "524519529379",
                appId: "1:524519529379:web:0cf27fa2c6b9258399d816",
                measurementId: "G-EF8X6XP8W6"
              };
        
            // Initialize Firebase
            firebase.initializeApp(firebaseConfig);
        
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
                'size': 'invisible',
                'callback': function (response) {
                    // reCAPTCHA solved, allow signInWithPhoneNumber.
                    console.log('recaptcha resolved');
                }
            });
            onSignInSubmit();
        });
        
        
        
        function onSignInSubmit() {
            $('#verifPhNum').on('click', function() {
                let phoneNo = '';
                var code = $('#codeToVerify').val();
                console.log(code);
                $(this).attr('disabled', 'disabled');
                $(this).text('Processing..');
                confirmationResult.confirm(code).then(function (result) {
                            alert('Succecss');
                    var user = result.user;
                    console.log(user);
            
            
                    // ...
                }.bind($(this))).catch(function (error) {
                
                    // User couldn't sign in (bad verification code?)
                    // ...
                    $(this).removeAttr('disabled');
                    $(this).text('Invalid Code');
                    setTimeout(() => {
                        $(this).text('Verify Phone No');
                    }, 2000);
                }.bind($(this)));
            
            });
            
            
            $('#getcode').on('click', function () {
                var phoneNo = $('#number').val();
                console.log(phoneNo);
                // getCode(phoneNo);
                var appVerifier = window.recaptchaVerifier;
                firebase.auth().signInWithPhoneNumber(phoneNo, appVerifier)
                .then(function (confirmationResult) {
            
                    window.confirmationResult=confirmationResult;
                    coderesult=confirmationResult;
                    console.log(coderesult);
                }).catch(function (error) {
                    console.log(error.message);
            
                });
            });
        }
        